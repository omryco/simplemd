#include <pybind11/pybind11.h>
#include "interaction/interaction.hpp"


namespace py = pybind11;

PYBIND11_MODULE(interaction, module) {
    using namespace pybind11::literals;

    module.def("potential", &interaction::potential, "xi"_a)
    .def("force_magnitude", &interaction::force_magnitude, "xi"_a);

}