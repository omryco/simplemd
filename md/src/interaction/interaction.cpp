#include <cmath> 

#include "interaction.hpp"

namespace interaction{

    double constexpr xi_m = 1.737051787;

    double potential(double xi){
        if (xi <= 0.85) return -39170.516264*xi*xi*xi + 102795.709007*xi*xi - 90115.812549*xi + 26401.649473;
        else if (xi <= 1.24445506) return 4.*(1./std::pow(xi, 12) - 1./std::pow(xi, 6));
        else if (xi <= xi_m) return (3.2920028*(xi_m - xi)*(xi_m - xi)*(xi_m - xi) - 4.86489008*(xi_m - xi)*(xi_m - xi));
        else return 0.;
    }

    double force_magnitude(double xi){
        if (xi <= 0.85) return - (-39170.516264*3.*xi*xi + 102795.709007*2.*xi - 90115.812549);
        else if (xi <= 1.24445506) return -4.*(-12./std::pow(xi, 13) + 6./std::pow(xi, 7));
        else if (xi <= xi_m) return (3.2920028*3.*(xi_m - xi)*(xi_m - xi) - 4.86489008*2.*(xi_m - xi));
        else return 0.;
    }
}
