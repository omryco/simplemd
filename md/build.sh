cd "$(dirname "${BASH_SOURCE[0]}")"
rm -rf build
mkdir build
cd build
cmake ..
make -j
echo "from .src import *" > __init__.py
cd ../..
python3 -c "import md" # check for import success